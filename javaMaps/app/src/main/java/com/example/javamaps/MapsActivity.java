package com.example.javamaps;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        text=findViewById(R.id.editText);
    }
    double lat;
    double lon;
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
         return;
            }
            mMap.setMyLocationEnabled(true);
            LocationManager mng = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            Location location = mng.getLastKnownLocation(mng.getBestProvider(new Criteria(), false));
            lat = location.getLatitude();
            lon = location.getLongitude();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 10);
            mMap.animateCamera(cameraUpdate);
        }
        catch (Exception ex)
        {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        /*mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                // TODO Auto-generated method stub
                if(marker.equals(marker)){
                    Toast.makeText(getApplicationContext(), "test", Toast.LENGTH_LONG).show();
                    return true;
                }
                return false;
            }
        });*/

    }

    String latEnd;
    String lngEnd;
    LatLng marker;
    public void click(final View view) {
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin="+lat+","+lon+"&destination="+text.getText()+"&key=AIzaSyC_L6CzUfvRY6Kx8TQgIfJVU1Jz69OZbnc";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray routes = response.getJSONArray("routes");
                    JSONObject index1=routes.getJSONObject(0);
                    JSONArray legs=index1.getJSONArray("legs");
                    JSONObject index2=legs.getJSONObject(0);
                    JSONArray steps=index2.getJSONArray("steps");
                    Integer count=index2.getJSONArray("steps").length();
                    Toast.makeText(getApplicationContext(), count.toString(), Toast.LENGTH_LONG).show();
                    for(int i=0;i<count;i++) {
                        JSONObject index3 = steps.getJSONObject(i);
                        JSONObject start_location = index3.getJSONObject("start_location");
                        String latStart = start_location.getString("lat");
                        String lngStart = start_location.getString("lng");
                        JSONObject end_location = index3.getJSONObject("end_location");
                        latEnd = end_location.getString("lat");
                        lngEnd = end_location.getString("lng");
                        Polyline polyline1 = mMap.addPolyline(new PolylineOptions()
                                .clickable(true)
                                .add(
                                        new LatLng(Double.valueOf(latStart), Double.valueOf(lngStart)),
                                        new LatLng(Double.valueOf(latEnd), Double.valueOf(lngEnd))));
                    }
                    marker = new LatLng(Double.valueOf(latEnd), Double.valueOf(lngEnd));
                    mMap.addMarker(new MarkerOptions()
                            .position(marker)
                            .title(text.getText().toString()));
                } catch (JSONException e) {
                   text.setText(e.getMessage());
                }
            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);

    }
}
